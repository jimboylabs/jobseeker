<?php

use App\Http\Controllers\Api\CandidatesController;
use App\Http\Controllers\Api\VacanciesController;
use App\Http\Controllers\Api\VacancyCandidatesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('candidates', CandidatesController::class);

Route::apiResource('vacancies', VacanciesController::class);

Route::get('/vacancies/{vacancy}/candidates', [VacancyCandidatesController::class, 'index']);
Route::post('/vacancies/{vacancy}/candidates', [VacancyCandidatesController::class, 'store']);
