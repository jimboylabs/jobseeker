<?php

namespace App\Http\Controllers\Api;

use App\Enums\Gender;
use App\Http\Controllers\Controller;
use App\Http\Resources\CandidateResource;
use App\Models\Candidate;
use Illuminate\Validation\Rules\Enum;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $keywords = request('search');

        $query = Candidate::query()
            ->when($keywords, function ($q) use ($keywords) {
                $q->where('full_name', 'like', "%$keywords%")
                    ->orWhere('gender', 'like', "%$keywords%");
            });

        $candidateQuery = QueryBuilder::for($query)
            ->allowedFilters([
                'full_name',  
                AllowedFilter::exact('dob'),
                AllowedFilter::exact('gender'),
            ])
            ->allowedSorts(['id', 'full_name', 'gender']);

        return CandidateResource::collection($candidateQuery->get());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        $candidate = Candidate::where('full_name', request('full_name'))->first();

        abort_unless(is_null($candidate), 422);

        request()->validate([
            'full_name' => ['required', 'max:150'],
            'gender' => ['required', new Enum(Gender::class)],
            'dob' => ['required', 'date'],
        ]);

        $candidate = Candidate::create(request([
            'full_name',
            'gender',
            'dob',
        ]));

        return new CandidateResource($candidate);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(string $id)
    {
        $candidate = Candidate::findOrFail($id);

        request()->validate([
            'full_name' => ['required', 'max:150'],
            'gender' => ['required', new Enum(Gender::class)],
            'dob' => ['required', 'date'],
        ]);

        $candidate->update(request([
            'full_name',
            'gender',
            'dob',
        ]));

        return new CandidateResource($candidate);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $candidate = Candidate::findOrFail($id);

        $candidate->delete();

        return response('', 204);
    }
}
