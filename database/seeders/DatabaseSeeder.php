<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    protected function tables()
    {
        return collect(['candidates', 'vacancies', 'candidate_apply']);
    }

    protected function truncateTables()
    {
        Schema::disableForeignKeyConstraints();
        $this
          ->tables()
          ->each(function ($table) {
              DB::table($table)->truncate();
              $this->command->info("Truncated: {$table}");
          });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        if (app()->environment('production')) {
            exit("I just stopped you getting fired.");
        }

        $this->truncateTables();

        \App\Models\Candidate::factory(10)->create();
        \App\Models\Vacancy::factory(10)->create();

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
