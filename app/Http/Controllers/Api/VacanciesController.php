<?php

namespace App\Http\Controllers\Api;

use App\Enums\Gender;
use App\Http\Controllers\Controller;
use App\Http\Resources\VacancyResource;
use App\Models\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Enum;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class VacanciesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $keywords = request('search');

        $query = Vacancy::query()
            ->when($keywords, function ($q) use ($keywords) {
                $q
                    ->where('name', 'like', "%$keywords%")
                    ->orWhere('requirement_gender', 'like', "%$keywords%")
                    ->orWhere('max_age', 'like', "%$keywords%")
                    ->orWhere('min_age', 'like', "%$keywords%");
            });

        $vacancyQuery = QueryBuilder::for($query)
            ->allowedFilters([
                'name', 
                'min_age',
                'max_age',
                AllowedFilter::exact('requirement_gender'),
            ])
            ->allowedSorts(['id', 'name', 'requirement_gender', 'min_age', 'max_age']);

        return VacancyResource::collection($vacancyQuery->get());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        $vacancy = Vacancy::whereName(request('name'))->first();

        abort_unless(is_null($vacancy), 422);

        request()->validate([
            'name' => ['required', 'max:150'],
            'min_age' => ['required', 'integer', 'between:20,90'],
            'max_age' => ['required', 'integer', 'between:20,90'],
            'requirement_gender' => [new Enum(Gender::class)],
        ]);

        $vacancy = Vacancy::create(request([
            'name',
            'min_age',
            'max_age',
            'requirement_gender',
        ]));

        return new VacancyResource($vacancy);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(string $id)
    {
        $vacancy = Vacancy::findOrFail($id);

        request()->validate([
            'name' => ['required', 'max:150'],
            'min_age' => ['required', 'integer', 'between:20,90'],
            'max_age' => ['required', 'integer', 'between:20,90'],
            'requirement_gender' => [new Enum(Gender::class)],
        ]);

        $vacancy->update(request([
            'name',
            'min_age',
            'max_age',
            'requirement_gender',
        ]));

        return new VacancyResource($vacancy);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $vacancy = Vacancy::findOrFail($id);

        $vacancy->delete();

        return response('', 204);
    }
}
