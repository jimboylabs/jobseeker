
# Jobseeker

Go get a job!

## TODO

- [x] Fetch All Candidates 
- [x] Create a new Candidate
- [x] Search & Sort Candidates
- [x] Delete Candidate 
- [x] Update Candidate

- [x] Fetch All Vacancies
- [x] Create a new Vacancy
- [x] Search & Sort Vacancies
- [x] Delete Vacancy
- [x] Update Vacancy

- [X] Create a New Vacancy Application
- [X] List Candidate Apply


## Installation

You will need to have the following installed, before proceeding

- PHP 8.1+
- Composer
- [Docgen](https://github.com/thedevsaddam/docgen)
    
## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:jimboylabs/jobseeker.git
```

Go to the project directory

```bash
  cd jobseeker
```

Install dependencies

```bash
  composer install
```

Run MySQL server

```bash
  docker compose up
```

Create  `.env`

```bash
  cp .env.example .env
  php artisan key:generate
```

Configure `.env` for database connection

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=jobseeker
DB_USERNAME=root
DB_PASSWORD=root
```

Run database migration and seeder

```bash
    php artisan migrate --seed
```

Start the server

```bash
    php artisan serve
```

## Documentation

Go to the documentation directory

```bash
  cd docs
```

Run simple static server using php

```bash
    php -S localhost:8001
```

Open documentation in http://localhost:8001
