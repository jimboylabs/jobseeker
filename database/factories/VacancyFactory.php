<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Vacancy>
 */
class VacancyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => "Vacanci: " . fake()->name(),
            'min_age' => collect([0, 10, 20])->random(),
            'max_age' => collect([20, 30, 40])->random(),
            'requirement_gender' => collect(['male', 'female', null])->random(),
        ];
    }
}
