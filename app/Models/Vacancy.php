<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Vacancy extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'min_age',
        'max_age',
        'requirement_gender',
    ];

    public function candidates(): BelongsToMany
    {
        return $this
            ->belongsToMany(Candidate::class, 'candidate_apply')
            ->withPivot('apply_date')
            ->withTimestamps();
    }
}
