<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CandidateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'dob' => $this->dob,
            'gender' => $this->gender,
            'apply_date' => $this->whenPivotLoaded('candidate_apply', function () {
                return $this->pivot->apply_date;
            }),
        ];
    }
}
