<?php

namespace App\Http\Controllers\Api;

use App\Models\Candidate;
use App\Models\Vacancy;
use App\Http\Controllers\Controller;
use App\Http\Resources\CandidateResource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class VacancyCandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Vacancy $vacancy)
    {
        $keywords = request('search');

        $query = $vacancy
            ->candidates()
            ->when($keywords, function (Builder $q) use ($keywords) {
                $q
                    ->where('full_name', 'like', "%$keywords%")
                    ->orWhere('gender', 'like', "%$keywords%");
            });

        $candidateQuery = QueryBuilder::for($query)
            ->allowedFilters([
                'full_name',
                AllowedFilter::exact('dob'),
                AllowedFilter::exact('gender'),
            ])
            ->allowedSorts(['id', 'full_name', 'gender']);

        return CandidateResource::collection($candidateQuery->get());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Vacancy $vacancy)
    {
        request()->validate([
            'candidate_id' => ['required'],
            'apply_date' => ['required', 'date'],
        ]);

        $candidate = Candidate::findOrFail(request('candidate_id'));

        $vacancy->candidates()->attach($candidate->id, request(['apply_date']));

        return response('', 204);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
